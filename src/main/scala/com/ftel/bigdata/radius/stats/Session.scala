package com.ftel.bigdata.radius.stats

import com.ftel.bigdata.conf.Configure
import com.ftel.bigdata.radius.RadiusParameters
import com.ftel.bigdata.radius.classify.LoadLog
import com.ftel.bigdata.utils.DateTimeUtil
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.joda.time.Days


case class Session(timestamp: Long, name: String, sessionID: String, sessionTime: Long, statusType: String){
  def apply(timestamp: Long, name: String, sessionID: String, sessionTime: Long, statusType: String): Session = new Session(timestamp, name, sessionID, sessionTime, statusType)

  override def toString: String = Array(timestamp, name, sessionID, sessionTime, statusType).mkString("\t")
}

object Session {
  val DATE_FORMAT = "yyyy-MM-dd"
  val DATE_FORMAT_FULL = "yyyy-MM-dd HH:mm:ss"
  val OUTPUT_PATH = "/data/radius/stats/"

  def main(args: Array[String]): Unit = {
    val flag = args(0)
    val day = args(1)
    val sc = Configure.getSparkContext

    flag match {
      case "daily" => run(sc, day)
      case "session" => runSession(sc, day)
      case "monthly" => {
        val dateTime = DateTimeUtil.create(s"$day-01", DateTimeUtil.YMD).dayOfMonth().withMinimumValue()
        val number = dateTime.dayOfMonth().getMaximumValue()
        (0 until number).map(x => {
          run(sc, dateTime.plusDays(x).toString(DateTimeUtil.YMD))
        })
      }
      case "range" => {
        val from = DateTimeUtil.create(day, DateTimeUtil.YMD)
        val to = DateTimeUtil.create(args(2), DateTimeUtil.YMD)
        val days = (0 until Days.daysBetween(from, to).getDays + 1)
        days.map(x => from.plusDays(x).toString(DateTimeUtil.YMD))
          .foreach(x => {
            run(sc, x)
          })
      }
      case "name-error" => {
        val dateTime = DateTimeUtil.create(s"$day-01", DateTimeUtil.YMD).dayOfMonth().withMinimumValue()
        val number = dateTime.dayOfMonth().getMaximumValue()
        (0 until number).foreach(x => {
          saveNameError(sc, dateTime.plusDays(x).toString(DateTimeUtil.YMD))
        })
      }
      case "name-error-month" => {
        saveNameErrorMonth(sc, day)
      }
      case "duration-hourly" => {
        runCalculateHour(sc, day)
      }
      case "duration-month" => {
        val dateTime = DateTimeUtil.create(s"$day-01", DateTimeUtil.YMD).dayOfMonth().withMinimumValue()
        val number = dateTime.dayOfMonth().getMaximumValue()
        (0 until number).foreach(x => {
          runCalculateHour(sc, dateTime.plusDays(x).toString(DateTimeUtil.YMD))
        })
      }
    }
  }

  def run(sc: SparkContext, day: String): Unit = {
    val rdd = mappingDuration(sc, day).cache()

    rdd.map(x => Array(day, x._1.name.toLowerCase, x._1.sessionID, x._2).mkString("\t"))
      .coalesce(32)
      .saveAsTextFile(s"$OUTPUT_PATH/$day/session-new")

    rdd.map(x => x._1.name.toLowerCase -> (x._1.timestamp, x._2))
      .groupByKey()
      .map(x => (x._1, calcHour(x._2.toArray).mkString(",")))
      .map(x => x.productIterator.mkString(","))
      .coalesce(1)
      .saveAsTextFile(s"$OUTPUT_PATH/$day/duration-hourly")
  }

  def runSession(sc: SparkContext, day: String): Unit = {
    val rdd = mappingDuration(sc, day)

    rdd.map(x => Array(day, x._1.name.toLowerCase, x._1.sessionID, x._2).mkString("\t"))
      .coalesce(32)
      .saveAsTextFile(s"$OUTPUT_PATH/$day/session-new")
  }

  def runCalculateHour(sc: SparkContext, day: String): Unit = {
    val rdd = mappingDuration(sc, day)

    rdd.map(x => x._1.name.toLowerCase -> (x._1.timestamp, x._2))
      .groupByKey()
      .map(x => (x._1, calcHour(x._2.toArray).mkString(",")))
      .map(x => x.productIterator.mkString(","))
      .coalesce(1)
      .saveAsTextFile(s"$OUTPUT_PATH/$day/duration-hourly")
  }

  def saveNameError(sc: SparkContext, day: String): Unit = {
    val spark = SparkSession.builder().getOrCreate()
    import spark.implicits._

    val df = spark.read
      .option("delimiter", "\t")
      .option("inferSchema", "true")
      .csv(s"$OUTPUT_PATH/$day/session-new")
      .toDF("day", "name", "sessionID", "duration")

      df.filter($"duration" <= 86400)
        .groupBy($"name")
        .agg(sum("duration").alias("duration"))
        .filter($"duration" >= 86400*2)
        .sort($"duration".asc)
        .select("name")
        .coalesce(1)
        .write
        .csv(s"$OUTPUT_PATH/$day/name-error")
  }

  def saveNameErrorMonth(sc: SparkContext, date: String): Unit = {
    sc.textFile(s"$OUTPUT_PATH/$date-*/name-error")
      .distinct(1)
      .saveAsTextFile(s"/data/radius/name-error/$date")
  }


  private def maxTime(iterable: Iterable[Session]) = {
    iterable.map(_.timestamp).max
  }

  private def minTime(iterable: Iterable[Session]) = {
    iterable.map(_.timestamp).min
  }

  private def getLogLastByTime = (x: LoadLog, y: LoadLog) => {
    if (x.timestamp > y.timestamp) x else y
  }

  private def calcHour(pairs: Array[(Long, Long)]) = {
    val arr = new Array[Long](24)

    def calc(dateMil: Long, duration: Long) = {
      val d = DateTimeUtil.create(dateMil/1000)
      var dur = duration
      var i = 1

      while (dur>3600) {
        if (d.minusHours(i).getHourOfDay == 23) {
          dur = 0
          arr(0) = 0
        } else {
          val t = d.minusHours(i).getHourOfDay
          arr(t) = 3600
          i+= 1
          dur -= 3600
        }
      }
      arr(d.getHourOfDay) = dur
      arr
    }

    pairs.foreach(x => calc(x._1, x._2))
    arr
  }

  private def mappingDuration(sc: SparkContext, day: String) = {
    val path = RadiusParameters.PARTITION_PATH +  s"/load/${day}"

    val loadStats = sc.textFile(path, 1)
      .map(x => LoadLog(x))
      .filter(x => x.input >= 0 && x.output >= 0)
      .filter(x => x.statusType != "UNKNOWN")
      .map(x => (x.name + x.sessionID) -> x)
      .reduceByKey(getLogLastByTime)
      .map(x => x._2)
      .map(x => new Session(x.timestamp, x.name, x.sessionID, x.sessionTime, x.statusType))
      .persist()

    loadStats.groupBy(_.name)
      .flatMap(x => {
        val max = maxTime(x._2)
        val min = minTime(x._2)
        x._2.map(y => {
          if (min == max ) {
            (y, "onlyone")
          } else if (y.timestamp == max) {
            (y, "last")
          } else if (y.timestamp == min) {
            (y, "first")
          } else {
            (y, "middle")
          }
        })
      }).map(x => (x._1, x._2))
      .map(x => {
        val duration = if (x._2 == "onlyone") {
          if (x._1.statusType == LoadLog.ACTIVE) {
            86400
          } else {
            (x._1.timestamp - DateTimeUtil.create(day + " 00:00:00", DATE_FORMAT_FULL).getMillis) / 1000
          }
        } else if (x._2 == "first") {
          val maxSecond = (x._1.timestamp - DateTimeUtil.create(day + " 00:00:00", DATE_FORMAT_FULL).getMillis) / 1000
          if (x._1.sessionTime > maxSecond) maxSecond else x._1.sessionTime
        } else if (x._2 == "last") {
          if (x._1.sessionTime > 86400) {
            if (x._1.statusType == LoadLog.ACTIVE) {
              0
            } else {
              (x._1.timestamp - DateTimeUtil.create(day + " 00:00:00", DATE_FORMAT_FULL).getMillis) / 1000
            }
          } else {
            if (x._1.statusType == LoadLog.ACTIVE) {
              x._1.sessionTime + ((DateTimeUtil.create(day + " 23:59:59", DATE_FORMAT_FULL).getMillis - x._1.timestamp) / 1000)
            } else x._1.sessionTime
          }
        } else {
          if (x._1.sessionTime > 86400) 0 else x._1.sessionTime
        }
        (x._1, duration)
      })
  }

}
