package com.ftel.bigdata.radius.stats

import com.ftel.bigdata.conf.Configure
import com.ftel.bigdata.radius.RadiusParameters
import com.ftel.bigdata.radius.classify.LoadLog
import com.ftel.bigdata.utils.{DateTimeUtil, HdfsUtil}
import org.apache.hadoop.fs.FileSystem
import org.apache.spark.SparkContext
import org.apache.spark.sql.SparkSession
import org.joda.time.Days

object Driver {

  def main(args: Array[String]) {
    run(args)
  }
  
  private def run(args: Array[String]) {
    val flag = args(0)
    val day = args(1)

    val sc = Configure.getSparkContext()
    val sparkSession = SparkSession.builder().getOrCreate()

    flag match {
      case "range" => {
        val from = DateTimeUtil.create(day, DateTimeUtil.YMD)
        val to = DateTimeUtil.create(args(2), DateTimeUtil.YMD)
        
        val days = (0 until Days.daysBetween(from, to).getDays + 1)
        days.map(x => from.plusDays(x).toString(DateTimeUtil.YMD))
            .foreach(x => {
              println(s"====== Processing for $x ==========")
              run(sparkSession, x)
              println(s"====== Finished for $x ==========")
            })
      }
      
      case "month" => {
        val dateTime = DateTimeUtil.create(day, DateTimeUtil.YMD).dayOfMonth().withMinimumValue()
        val number = dateTime.dayOfMonth().getMaximumValue()
        (0 until number).map(x => {
          run(sparkSession, dateTime.plusDays(x).toString(DateTimeUtil.YMD))
        })
      }
      case "day" => {
        run(sparkSession, day)
      }
      case "diff" => {
        Difference.save(sparkSession, day)
      }
      case "diff-month" => {
        val dateTime = DateTimeUtil.create(day, DateTimeUtil.YMD).dayOfMonth().withMinimumValue()
        val number = dateTime.dayOfMonth().getMaximumValue()
        (0 until number).map(x => {
          Difference.save(sparkSession, dateTime.plusDays(x).toString(DateTimeUtil.YMD))
        })
      }

      case "feature" => {
        Feature.save(sparkSession, args(1))
      }
      case "feature-new" => {
        Feature.saveNew(sparkSession, args(1))
      }
      case "feature-merge" => {
        Feature.mergeFeatureFiles(sparkSession, args(1))
      }
      case "feature-merge-new" => {
        Feature.mergeFeatureFilesNew(sparkSession, args(1))
      }
      case "contract" => {
        val output = s"/data/radius/stats/${day}"
        val loadStats = sc.textFile(output + s"/load-usage", 1).map(x => new LoadStats(x))
        val count = loadStats.map(x => x.name).distinct().count()
        println("COUNT: " + count)
        
      }
      case "metric-month" => {
        val dateTime = DateTimeUtil.create(day, DateTimeUtil.YMD).dayOfMonth().withMinimumValue()
        val number = dateTime.dayOfMonth().getMaximumValue()
        (0 until number).map(x => {
          Metric.save(sparkSession, dateTime.plusDays(x).toString(DateTimeUtil.YMD))
        })
      }
    }
  }

  private def run(sparkSession: SparkSession, day: String): Unit = {
    calLoadUsage(sparkSession, sparkSession.sparkContext, day)
    val rdd = Metric.cal(sparkSession, sparkSession.sparkContext, day)
    Session.run(sparkSession.sparkContext, day)
    DownUp.save(rdd, day)
    Difference.save(sparkSession, day)
  }

  private def calLoadUsage(sparkSession: SparkSession, sc: SparkContext, day: String) {
    val fs = FileSystem.get(sc.hadoopConfiguration)
    val path = RadiusParameters.PARTITION_PATH +  s"/load/${day}"
    val output = RadiusParameters.STATS_PATH +  s"/${day}/load-usage"

    if (!HdfsUtil.isExist(fs, output)) {
      val loadStats = sc.textFile(path, 1).map(x => LoadLog(x)).map(x => LoadStats(x))
      Functions.calculateLoad(sparkSession, loadStats).coalesce(32, false, None).saveAsTextFile(output + s"/load-usage")
    } else {
      println(s"=====> This $output have already exist!")
    }
  }

}