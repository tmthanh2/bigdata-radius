package com.ftel.bigdata.radius.es

import com.ftel.bigdata.conf.Configure
import com.ftel.bigdata.radius.classify.ConLog
import com.ftel.bigdata.utils.{DateTimeUtil, Parameters, StringUtil}
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.{SparkConf, SparkContext}
import org.elasticsearch.spark.sql.sparkDataFrameFunctions
import org.apache.spark.sql.functions._

object DwhRadiusUser {
  val SPARK_MASTER = "yarn"
  val OUTPUT_PATH = "/data/radius/user-daily"

  def main(args: Array[String]): Unit = {
    val sc = new SparkContext(createSparkConf())
    sc.hadoopConfiguration.set("fs.file.impl", classOf[org.apache.hadoop.fs.LocalFileSystem].getName())
    sc.hadoopConfiguration.set(Parameters.SPARK_READ_DIR_RECURSIVE, "true")

    val day = args(0)

    run(sc, day)
//    runLocal(Configure.getSparkContextLocal, "")
  }

  def run(sc: SparkContext, day: String): Unit = {
    val path = s"/data/radius/partition/con/${day}"

    val rdd = sc.textFile(path, 1)
      .filter(x => StringUtil.isNotNullAndEmpty(x))
      .map(x => ConLog(x))

    writeUserDaily(rdd, day)
  }

  def runLocal(sc: SparkContext, day: String): Unit = {
    val path = s"testlog.csv"

    val rdd = sc.textFile(path, 1)
      .filter(x => StringUtil.isNotNullAndEmpty(x))
      .map(x => ConLog(x))

    val last = lastByTypeTheHardWay(rdd, day)
    val min = groupByMinute(rdd)
    min.join(last, Seq("bras_id", "name"))
      .select("date", "bras_id", "name", "reject", "signin", "logoff", "last_reject", "last_signin", "last_logoff")
      .show()

//          .write
//          .csv(s"$OUTPUT_PATH/$day")
  }

  def writeUserDaily(rdd: RDD[ConLog], day: String): Unit = {
    val last = lastByType(rdd)
    val min = groupByMinute(rdd)
    min.join(last, Seq("bras_id", "name"))
      .select("date", "bras_id", "name", "reject", "signin", "logoff", "last_reject", "last_signin", "last_logoff")
      .coalesce(1)
      .write
      .option("header", "true")
      .csv(s"$OUTPUT_PATH/$day")
  }


  def groupByMinute(rdd: RDD[ConLog]): DataFrame = {
    val spark = SparkSession.builder().getOrCreate()
    import spark.implicits._

    rdd.map(x => (
      DateTimeUtil.create(x.timestamp / 1000).toString("yyyy-MM-dd HH"),
      x.nasName.toUpperCase(),
      x.name,
      x.typeLog))
    .toDF("date", "bras_id", "name", "type")
    .groupBy("date", "bras_id", "name")
    .pivot("type")
    .count()
    .na.fill(0)
    .select("date", "bras_id", "name", "Reject", "SignIn", "LogOff")
    .toDF("date", "bras_id", "name", "reject", "signin", "logoff")

//      .write
//      .csv(s"$OUTPUT_PATH/$day")
//      .saveToEs(s"radius-user-daily-$day/docs")
  }

  def lastByTypeTheHardWay(rdd: RDD[ConLog], day: String): DataFrame = {
    val spark = SparkSession.builder().getOrCreate()
    import spark.implicits._

    val df = rdd.map(x => (
      x.timestamp,
      x.nasName.toUpperCase(),
      x.name,
      x.typeLog))
      .toDF("date", "bras_id", "name", "type")

    val windowSpec = Window.partitionBy("bras_id", "name", "type")
      .orderBy($"date".desc)

    df.withColumn("row_number", row_number().over(windowSpec))
      .filter($"row_number" === 1)
      .groupBy("bras_id", "name")
      .pivot("type")
      .max("date")
      .na.fill(0)
      .select("bras_id", "name", "Reject", "SignIn", "LogOff")
        .rdd.map(x => {
          val reject = if (x.getLong(2) == 0) "0" else DateTimeUtil.create(x.getLong(2)/1000).toString("HH:mm")
          val signIn = if (x.getLong(3) == 0) "0" else DateTimeUtil.create(x.getLong(3)/1000).toString("HH:mm")
          val logOff = if (x.getLong(4) == 0) "0" else DateTimeUtil.create(x.getLong(4)/1000).toString("HH:mm")
          (x.getString(0), x.getString(1), reject, signIn, logOff)
        })
      .toDF("bras_id", "name", "last_reject", "last_signin", "last_logoff")
  }

  def lastByType(rdd: RDD[ConLog]): DataFrame = {
    val spark = SparkSession.builder().getOrCreate()
    import spark.implicits._

    rdd.map(x => (x.timestamp, x.nasName.toUpperCase(), x.name, x.typeLog))
      .toDF("date", "bras_id", "name", "type")
      .groupBy("bras_id", "name")
      .pivot("type")
      .max("date")
      .select("bras_id", "name", "Reject", "SignIn", "LogOff")
      .na.fill(0)
      .rdd.map(x => {
        val reject = if (x.getLong(2) == 0) "" else DateTimeUtil.create(x.getLong(2)/1000).toString("HH:mm")
        val signIn = if (x.getLong(3) == 0) "" else DateTimeUtil.create(x.getLong(3)/1000).toString("HH:mm")
        val logOff = if (x.getLong(4) == 0) "" else DateTimeUtil.create(x.getLong(4)/1000).toString("HH:mm")
        (x.getString(0), x.getString(1), reject, signIn, logOff)
      })
      .toDF("bras_id", "name", "last_reject", "last_signin", "last_logoff")
  }

  def createSparkConf(): SparkConf = {
    val sparkConf = new SparkConf().setAppName("DWH_RADIUS_USER").setMaster(SPARK_MASTER)
    sparkConf.set("es.nodes", "172.27.11.156" + ":" + "9200") // List IP/Hostname/host:port
    //sparkConf.set("es.port", port)  // apply for host in es.nodes that do not have any port specified
    // For ES version 5.x, Using 'create' op will error if you don't set id when create
    // To automatic ID generate, using 'index' op
    sparkConf.set("es.write.operation", "index")
    sparkConf.set("es.batch.size.bytes", "10mb")
    sparkConf.set("es.batch.size.entries", Integer.toString(1000)) // default 1000
    sparkConf.set("es.batch.write.refresh", "true")
    sparkConf
  }

}
