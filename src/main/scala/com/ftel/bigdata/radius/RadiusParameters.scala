package com.ftel.bigdata.radius

object RadiusParameters {
  private val ROOT_PATH = "/data/radius"
  val RAW_PATH = ROOT_PATH + "/rawlog"
  val CLASSIFY_PATH = ROOT_PATH + "/classify"
  val STATS_PATH = ROOT_PATH + "/stats"
  val FEATURE_PATH = ROOT_PATH + "/feature"
  val TEMP_PATH = ROOT_PATH + "/temp"
  val PARTITION_PATH = ROOT_PATH + "/partition"

  val ES_HOST = "172.27.11.151"
  val ES_PORT = 9200
}