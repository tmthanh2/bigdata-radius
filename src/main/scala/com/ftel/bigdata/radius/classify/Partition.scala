package com.ftel.bigdata.radius.classify

import com.ftel.bigdata.conf.Configure
import com.ftel.bigdata.utils.HdfsUtil
import org.apache.hadoop.fs.FileSystem
import org.apache.spark.SparkContext

object Partition {
  val INPUT_PATH = "/data/radius/partition/load"
  val OUTPUT_PATH = "/data/radius/classify"

  def main(args: Array[String]): Unit = {
    val day = args(0)
    val sc = Configure.getSparkContext()

    run(sc, day)
  }

  def run(sc: SparkContext, day: String): Unit = {
    val fs = FileSystem.get(sc.hadoopConfiguration)

    for (i <- 0 to 9) {
      val path = s"$INPUT_PATH/$day/0$i"
      if (HdfsUtil.isExist(fs, path)) {
        sc.textFile(path).saveAsTextFile(s"$OUTPUT_PATH/day=$day/type=load/hour=0$i")
      }
    }

    for (i <- 10 to 23) {
      val path = s"$INPUT_PATH/$day/$i"
      if (HdfsUtil.isExist(fs, path)) {
        sc.textFile(path).saveAsTextFile(s"$OUTPUT_PATH/day=$day/type=load/hour=$i")
      }
    }
  }

}
