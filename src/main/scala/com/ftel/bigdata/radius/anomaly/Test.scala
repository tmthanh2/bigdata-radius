package com.ftel.bigdata.radius.anomaly

import com.ftel.bigdata.utils.DateTimeUtil
import org.apache.spark.sql.SparkSession
import org.apache.spark.{SparkConf, SparkContext}

case class Rational(x: Int, y: Int) {
  assert(y > 0, "denominator must be positive")
  private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
  var numer = x / gcd(x, y)
  var denom = y / gcd(x, y)

  override def toString: String = numer + "/" + denom
}

object Test {
  val DATE_FORMAT = "yyyy-MM-dd"
  def plus(a:Int, b:Int) = a + b

  def main(args: Array[String]): Unit = {
//    val spark = SparkSession.builder().master("local[*]").getOrCreate()
//    spark.sparkContext.setLogLevel("INFO")
//    import spark.implicits._
//    import org.apache.spark.sql.functions._
//
//    val df1 = spark.createDataset(Seq(("2018-04-12", 2), ("2018-04-13", 2))).toDF("c1", "c2")
//    val active = spark.createDataset(Seq(("a", 2), ("a", 2))).toDF("c1", "c2b")
//
//    val df3 = df1.withColumn("c3", $"c1".cast(DateType))
//
//    df3.show()
//    df3.printSchema()

//    val from = DateTimeUtil.create("2018-04-01", DateTimeUtil.YMD)
//    val to = DateTimeUtil.create("2018-04-19", DateTimeUtil.YMD)
//    val days = (0 until Days.daysBetween(from, to).getDays + 1)
//    days.map(x => from.plusDays(x).toString(DateTimeUtil.YMD))
//      .foreach(x => {
//        println(x)
//      })
//    println(DateTimeUtil.create(1526109863114L/1000).toString)
    val dateTime = DateTimeUtil.create("2018-03-01", DateTimeUtil.YMD).dayOfMonth().withMinimumValue()
    val number = dateTime.dayOfMonth().getMaximumValue()
    (0 to number).foreach(x => {
      println(x)
    })
  }

  def streaming(): Unit = {
    val sparkConf = new SparkConf().setAppName("Local").setMaster(s"local[8]")
      .set("spark.sql.streaming.checkpointLocation", "data/checkpoint")

    val sc = new SparkContext(sparkConf)

    val spark = SparkSession.builder().getOrCreate()
    import spark.implicits._

    val brokers: String = "172.27.11.75:9092,172.27.11.80:9092,172.27.11.85:9092"
    val topic: String = "radius"

    val df = spark
      .readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", brokers)
      .option("assign", s""" {"$topic":[0,1,2]}""")
      .option("startingOffsets", "latest")
      .load()

    val df2 = df.selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)")
      .as[(String, String)]


    val query = df2.writeStream
      //      .outputMode("complete")
      .format("csv")
      .option("path", "data")
      .start()

    query.awaitTermination()

  }
}
