package com.ftel.bigdata.radius.anomaly

import com.ftel.bigdata.conf.Configure
import com.ftel.bigdata.utils.DateTimeUtil
import org.apache.spark.sql.{SaveMode, SparkSession}

object Threshold {
  val ROOT_PATH = "/data/radius/stats-con"
  val OUTPUT_PATH = "/data/radius/threshold-week"
  val LOCAL_PATH = "/home/thanhtm/2018-03-??"

  def generatePath(day: String) = {
    var path = s"$day,"
    for (i <- 1 to 6) {
      val t = DateTimeUtil.create(day, DateTimeUtil.YMD).minusDays(i).toString(DateTimeUtil.YMD)
      path += s"$t,"
    }
    path.dropRight(1)
  }

  def main(args: Array[String]): Unit = {
    val sc = Configure.getSparkContext()
    val spark = SparkSession.builder().getOrCreate()
    import spark.implicits._

    val day = args(0)
    val path = generatePath(day)

    sc.textFile(s"$ROOT_PATH/{$path}")
      .map(_.split("\t"))
      .filter(_.length == 9)
      .map(x => (x(6), x(8)))
      .toDF("signIn", "logOff")
      .select($"signIn"./($"logOff").alias("SL"), $"logOff"./($"signIn").alias("LS"))
      .na.drop()
      .createOrReplaceTempView("table")

    val threshold = spark.sql(s"SELECT PERCENTILE(SL, 0.95), PERCENTILE(LS, 0.95) FROM table").collect()

    sc.makeRDD(threshold, 1)
      .map(x => (x.getDouble(0).toInt, x.getDouble(1).toInt))
      .map(_.productIterator.mkString("\t"))
      .toDF()
      .write
      .mode(SaveMode.Overwrite)
      .csv(OUTPUT_PATH)
  }
}
