package com.ftel.bigdata.radius.anomaly

import com.ftel.bigdata.conf.Configure
import com.ftel.bigdata.radius.postgres.PostgresUtil
import com.ftel.bigdata.radius.utils.Math
import com.ftel.bigdata.utils.DateTimeUtil
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._


object LocalDriver {
  val DATE_FORMAT = "yyyy-MM-dd HH:mm"

  def getCurrentTime() = {
    DateTimeUtil.now.toString("yyyy-MM-dd HH-mm")
  }

  def IQR(S: Int, L: Int) = {
    val SLRate: Double = S/L
    val LSRate: Double = L/S
    val SLRate15m = Array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15)
    val LSRate15m = Array(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15)
    if (SLRate > LSRate) {
      (4* Math.quantile(SLRate15m, 75) - Math.quantile(SLRate15m, 25)).toDouble

    } else {
      (4* Math.quantile(LSRate15m, 75) - Math.quantile(LSRate15m, 25)).toDouble
    }
  }

  def main(args: Array[String]): Unit = {
    val sc = Configure.getSparkContextLocal
    val spark = SparkSession.builder().getOrCreate()
    import spark.implicits._
    val table = "dwh_conn_port"

    val conn = PostgresUtil.getFromPostgres(table)

    val dfTemp = conn.toDF("date_time", "bras_id", "linecard", "card", "port", "signInUnique", "signIn", "logoffUnique", "logOff")
      .select("date_time", "bras_id", "signIn", "logOff")
      .filter($"signIn" =!= 0 && $"logOff" =!= 0)
      .repartition(32)
      .cache()

    val act_user = PostgresUtil.getFromPostgres("active_user").rdd.map(x => (x.getString(0).toLowerCase, x.getInt(1)))
      .toDF("bras_id", "active_users").cache()

    act_user.filter(p => p.getString(0) == "bnh-mp-01-03")
      .show()

    var t = "2018-03-18 11:03"
//    val prev15m = DateTimeUtil.create(t, DATE_FORMAT).minusMinutes(15).toString(DATE_FORMAT)
//    dfTemp.filter($"date_time".between(prev15m, "2018-03-18 11:05"))
//      .filter($"bras_id" === "PTO-MP-01-01")
//      .groupBy($"date_time", $"bras_id")
//      .agg(sum($"signIn"), sum($"logOff").alias("logOff"))
//      .sort($"logOff")
//      .show(100)

//    for (i <- 1 until 1000) {
//      println(t)
//      test(t)
//      t = DateTimeUtil.create(t, DATE_FORMAT).plusMinutes(1).toString(DATE_FORMAT)
//    }

//    test(t)

    def test(t: String): Unit = {
      val prev15m = DateTimeUtil.create(t, DATE_FORMAT).minusMinutes(15).toString(DATE_FORMAT)


      val previousBrasLog = dfTemp.filter($"date_time".between(prev15m, t))//.filter($"bras_id" === brasId)
        .groupBy($"date_time", $"bras_id")
        .agg(sum($"signIn").alias("signIn"), sum($"logOff").alias("logOff"))
        .rdd
        .map(x => (x.getString(1)) -> (x.getLong(2), x.getLong(3)))
        .groupByKey()
        .filter(_._2.size > 0)
        .mapValues(x => Math.IQR(x))
        .map(x => (x._1, x._2._1, x._2._2))
        .toDF("bras_id", "SL_IQR", "LS_IQR")

      val presentBrasLog = dfTemp.filter($"date_time" === t)//.filter($"bras_id" === brasId)
        .groupBy($"date_time", $"bras_id")
        .agg(sum($"signIn").alias("signIn"), sum($"logOff").alias("logOff"))
        .rdd
        .map(x => (x.getString(1), x.getLong(2), x.getLong(3), x.getLong(2)/x.getLong(3).toDouble, x.getLong(3)/x.getLong(2).toDouble))
        .toDF("bras_id", "signIn", "logOff", "SL_Rate", "LS_Rate")


      val joined = presentBrasLog.join(previousBrasLog, Seq("bras_id"))
        .join(act_user, Seq("bras_id"))

      val SThresh = 5
      val LThresh = 3
      val result = joined.rdd.map(x => {
        val bras_id = x.getString(0)
        val S = x.getLong(1)
        val L = x.getLong(2)
        val SL_Rate = x.getDouble(3)
        val LS_Rate = x.getDouble(4)
        val SL_IQR = x.getLong(5)
        val LS_IQR = x.getLong(6)
        val active_users = x.getInt(7)
        if (SL_Rate > LS_Rate) {
          if (S > SThresh && SL_Rate > SL_IQR && S/active_users.toDouble > 0.03) {
            (bras_id, S, L, SL_Rate, LS_Rate, SL_IQR, LS_IQR, active_users, "true")
          } else {
            (bras_id, S, L, SL_Rate, LS_Rate, SL_IQR, LS_IQR, active_users, "false")
          }
        } else {
          if (L > LThresh && LS_Rate > LS_IQR && L/active_users.toDouble > 0.03) {
            (bras_id, S, L, SL_Rate, LS_Rate, SL_IQR, LS_IQR, active_users, "true")
          } else {
            (bras_id, S, L, SL_Rate, LS_Rate, SL_IQR, LS_IQR, active_users, "false")
          }
        }
      })
        .toDF("bras_id", "signIn", "logOff", "SL_Rate", "LS_Rate", "SL_IQR", "LS_IQR", "active_users", "label")
        .filter($"label" === "true")

      if (result.count()>0) {
        result.show()
      }
    }
  }

}
