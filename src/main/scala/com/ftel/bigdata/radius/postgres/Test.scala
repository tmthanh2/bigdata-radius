package com.ftel.bigdata.radius.postgres

import java.sql
import java.text.SimpleDateFormat

import com.ftel.bigdata.conf.Configure
import com.ftel.bigdata.radius.anomaly.ADStreaming.DATE_FORMAT
import com.ftel.bigdata.utils.DateTimeUtil
import org.apache.spark.SparkContext
import org.apache.spark.sql.{DataFrame, SaveMode, SparkSession}
import org.apache.spark.sql.functions._

object Test {

  def main(args: Array[String]): Unit = {
    val sc = Configure.getSparkContextLocal
    val spark = SparkSession.builder().getOrCreate()
    import spark.implicits._

    val url = PostgresUtil.getURL()
    val tableBras = "dwh_conn_bras"
    val properties = PostgresUtil.getProperties()

    val connPort = PostgresUtil.getFromPostgres("(select * from dwh_conn_port where date_time between '2018-03-27' and '2018-03-28') as dwh_conn_port")
      .groupBy("date_time", "bras_id")
      .agg(sum($"signInUnique").alias("signInUnique"),
        sum($"signIn").alias("signIn"),
        sum($"logoffUnique").alias("logoffUnique"),
        sum($"logoff").alias("logoff"))
      .write
      .mode(SaveMode.Append)
      .jdbc(url, tableBras, properties)
  }
}
